package Sprint1;

import Sprint1.service.impl.EnrichmentServiceImpl;
import Sprint1.service.EnrichmentServiceResolver;
import Sprint1.service.impl.MsisdnEnrichedMessageFactoryImpl;
import Sprint1.models.EnrichedMessage;
import Sprint1.models.Message;
import Sprint1.models.MessageModel;
import org.json.JSONException;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.skyscreamer.jsonassert.JSONAssert;
import org.skyscreamer.jsonassert.JSONCompareMode;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Sprint1Test {

    private static final int THREAD_QUANTITY = 100;

    EnrichmentServiceImpl enrichmentServiceImpl = new EnrichmentServiceImpl(new EnrichmentServiceResolver());
    MsisdnEnrichedMessageFactoryImpl msisdnEnrichedMessageFactoryImpl = new MsisdnEnrichedMessageFactoryImpl();
    List<Message> messagesList = new ArrayList<>();

    @DisplayName("Позитивный тест. Успешное обогащение сообщения.")
    @ParameterizedTest(name = "Успешное обогащение сообщения. (msisdn = {0}, firstName = {1}, lastName = {2})")
    @CsvFileSource(resources = "successEnrichMessage.csv", numLinesToSkip = 1)
    public void successEnrichMessageTest(String msisdn, String firstName, String lastName) throws JSONException {
        MessageModel message = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .build();

        MessageModel rawExpectedMessage = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .setEnrichmentFirstName(firstName)
                .setEnrichmentLastName(lastName)
                .build();

        String expectedMessage = rawExpectedMessage.getSendMessage(rawExpectedMessage);
        EnrichedMessage actualEnrichedMessage = msisdnEnrichedMessageFactoryImpl.enrich(
                new Message(message.getSendMessage(message), Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(expectedMessage, actualEnrichedMessage.getContent(), JSONCompareMode.STRICT);
    }

    @DisplayName("Позитивный тест. Сообщение возвращается в том же виде, в котором пришло")
    @ParameterizedTest(name = "Сообщение возвращается в том же виде, в котором пришло. (msisdn = {0}, firstName = {1}, lastName = {2})")
    @CsvFileSource(resources = "successReturnOriginalMessage.csv", numLinesToSkip = 1)
    public void successReturnOriginalMessageTest(String msisdn, String firstName, String lastName) {
        MessageModel message = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .build();

        String expectedMessage = message.getSendMessage(message);
        EnrichedMessage actualMessage = msisdnEnrichedMessageFactoryImpl.enrich(
                new Message(message.getSendMessage(message), Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(expectedMessage, actualMessage.getContent(), JSONCompareMode.STRICT);
    }

    @DisplayName("Позитивный тест. Если поле enrichment уже есть в сообщении, оно перезаписывается.")
    @ParameterizedTest(name = "Если поле enrichment уже есть в сообщении, оно перезаписывается. (msisdn = {0}, firstName = {1}, lastName = {2})")
    @CsvFileSource(resources = "successAlreadyExistEnrichment.csv", numLinesToSkip = 1)
    public void successAlreadyExistEnrichmentTest(String msisdn, String firstName, String lastName) throws JSONException {
        MessageModel message = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .setEnrichmentFirstName("James")
                .setEnrichmentLastName("Bond")
                .build();

        MessageModel rawExpectedMessage = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .setEnrichmentFirstName(firstName)
                .setEnrichmentLastName(lastName)
                .build();

        String expectedMessage = rawExpectedMessage.getSendMessage(rawExpectedMessage);
        EnrichedMessage actualEnrichedMessage = msisdnEnrichedMessageFactoryImpl.enrich(
                new Message(message.getSendMessage(message), Message.EnrichmentType.MSISDN));
        JSONAssert.assertEquals(expectedMessage, actualEnrichedMessage.getContent(), JSONCompareMode.STRICT);
    }

    @DisplayName("Позитивный тест. Проверка добавления в список обогащенных/необогащённых сообщений.")
    @ParameterizedTest(name = "Проверка добавления в список обогащенных/необогащённых сообщений. (msisdn = {0}, enrichedMessagesListSize = {1}, unenrichedMessagesListSize = {2})")
    @CsvFileSource(resources = "addInMessageList.csv", numLinesToSkip = 1)
    public void successCheckToAddInMessageListTest(String msisdn, int enrichedMessagesListSize, int unenrichedMessagesListSize) {
        MessageModel message = new MessageModel.Builder()
                .setMsisdn(msisdn)
                .build();

        enrichmentServiceImpl.enrich(new Message(message.getSendMessage(message), Message.EnrichmentType.MSISDN));
        checkMessageListSize(enrichedMessagesListSize, unenrichedMessagesListSize);
    }

    @Test
    @DisplayName("Позитивный тест. Проверка обработки сообщений из нескольких потоков.")
    public void successEnrichMessageInDifferentThreadsTest() throws InterruptedException {
        MessageModel rawCorrectMessage = new MessageModel.Builder()
                .setMsisdn("88000000001")
                .setEnrichmentFirstName("TestUserName1")
                .setEnrichmentLastName("TestUserSurname1")
                .build();

        MessageModel rawNotCorrectMessage = new MessageModel.Builder()
                .setMsisdn("80000000000")
                .setEnrichmentFirstName("TestUserName0")
                .setEnrichmentLastName("TestUserSurname0")
                .build();

        String expectedCorrectMessage = rawCorrectMessage.getSendMessage(rawCorrectMessage);
        String expectedNotCorrectMessage = rawNotCorrectMessage.getSendMessage(rawNotCorrectMessage);

        messagesList.add(new Message(expectedCorrectMessage, Message.EnrichmentType.MSISDN));
        messagesList.add(new Message(expectedNotCorrectMessage, Message.EnrichmentType.MSISDN));

        ExecutorService service = Executors.newFixedThreadPool(10);
        CountDownLatch latch = new CountDownLatch(THREAD_QUANTITY);

        for (int i = 0; i < THREAD_QUANTITY; i++) {
            service.execute(() -> {
                Message successEnrichedMessage = messagesList.get(0);
                Message unenrichedMessage = messagesList.get(1);
                enrichmentServiceImpl.enrich(successEnrichedMessage);
                enrichmentServiceImpl.enrich(unenrichedMessage);
                latch.countDown();
            });
        }
        latch.await();
        checkMessageListSize(THREAD_QUANTITY, THREAD_QUANTITY);
        checkMessageInMessageList(expectedCorrectMessage, enrichmentServiceImpl.getEnrichedMessagesList());
        checkMessageInMessageList(expectedNotCorrectMessage, enrichmentServiceImpl.getUnenrichedMessagesList());
    }

    private void checkMessageInMessageList(String expectedMessage, List<String> messageList) {
        messageList.forEach((message) -> {
            try {
                JSONAssert.assertEquals(expectedMessage, message, JSONCompareMode.STRICT);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });
    }

    private void checkMessageListSize(int enrichedMessagesListSize, int unenrichedMessagesListSize) {
        Assertions.assertAll(
                () -> assertEquals(enrichedMessagesListSize, enrichmentServiceImpl.getEnrichedMessagesList().size()),
                () -> assertEquals(unenrichedMessagesListSize, enrichmentServiceImpl.getUnenrichedMessagesList().size())
        );
    }
}
