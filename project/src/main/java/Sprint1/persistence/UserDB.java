package Sprint1.persistence;

import Sprint1.models.Enrichment;

import java.util.HashMap;
import java.util.Map;

public class UserDB {

    private final Map<String, Enrichment> users = new HashMap<>() {{
        put("88000000001", new Enrichment("TestUserName1", "TestUserSurname1"));
        put("88000000002", new Enrichment("TestUserName2", "TestUserSurname2"));
        put("88000000003", new Enrichment("TestUserName3", "TestUserSurname3"));
        put("88000000004", new Enrichment("TestUserName4", "TestUserSurname4"));
        put("88000000005", new Enrichment("TestUserName5", "TestUserSurname5"));
    }};

    public Enrichment getUserByMsisdn(String msisdn) {
        return users.get(msisdn);
    }
}
