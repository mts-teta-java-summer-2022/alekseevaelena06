package Sprint1.service;

import Sprint1.models.Message;

public interface EnrichmentService {
    String enrich(Message message);
}
