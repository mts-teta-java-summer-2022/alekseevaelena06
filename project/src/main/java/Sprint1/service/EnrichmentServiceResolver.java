package Sprint1.service;

import Sprint1.service.impl.MsisdnEnrichedMessageFactoryImpl;
import Sprint1.models.Message;

import java.util.HashMap;
import java.util.Map;

public class EnrichmentServiceResolver {
    private final Map<Message.EnrichmentType, EnrichedMessageFactory> resolver;

    public EnrichmentServiceResolver() {
        this.resolver = new HashMap<>();
        resolver.put(Message.EnrichmentType.MSISDN, new MsisdnEnrichedMessageFactoryImpl());
    }

    public EnrichedMessageFactory resolveEnrichmentType(Message.EnrichmentType enrichmentType) {
        return resolver.get(enrichmentType);
    }
}
