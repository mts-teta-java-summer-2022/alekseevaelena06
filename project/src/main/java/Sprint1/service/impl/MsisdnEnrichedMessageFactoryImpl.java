package Sprint1.service.impl;

import Sprint1.service.EnrichedMessageFactory;
import Sprint1.models.Enrichment;
import Sprint1.models.Message;
import Sprint1.models.EnrichedMessage;
import Sprint1.persistence.UserDB;
import org.json.simple.JSONObject;

public class MsisdnEnrichedMessageFactoryImpl implements EnrichedMessageFactory {

    @Override
    public EnrichedMessage enrich(Message message) {
        JSONObject jsonMessage;

        try {
            jsonMessage = message.getJSONMessage();
            if (!jsonMessage.containsKey("msisdn")) {
                return new EnrichedMessage(message.getContent(), false);
            }
            setEnrichment(jsonMessage);
        } catch (Exception e) {
            return new EnrichedMessage(message.getContent(), false);
        }
        return new EnrichedMessage(jsonMessage.toString(), true);
    }

    public JSONObject getEnrichment(JSONObject jsonObject) throws NullPointerException {
        Enrichment user = new UserDB().getUserByMsisdn(String.valueOf(jsonObject.get("msisdn")));
        JSONObject enrichment = new JSONObject();
        enrichment.put("firstName", user.getFirstName());
        enrichment.put("lastName", user.getLastName());

        return enrichment;
    }

    public JSONObject setEnrichment(JSONObject jsonObject) throws NullPointerException {
        if (jsonObject.containsKey("enrichment")) jsonObject.replace("enrichment", getEnrichment(jsonObject));
        else {
            jsonObject.put("enrichment", getEnrichment(jsonObject));
        }
        return jsonObject;
    }
}
