package Sprint1.service.impl;

import Sprint1.service.EnrichedMessageFactory;
import Sprint1.service.EnrichmentService;
import Sprint1.service.EnrichmentServiceResolver;
import Sprint1.models.Message;
import Sprint1.models.EnrichedMessage;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EnrichmentServiceImpl implements EnrichmentService {
    private final EnrichmentServiceResolver enrichmentResolver;
    private final List<String> enrichedMessagesList;
    private final List<String> unenrichedMessagesList;

    public EnrichmentServiceImpl(EnrichmentServiceResolver enrichmentResolver) {
        this.enrichmentResolver = enrichmentResolver;
        enrichedMessagesList = Collections.synchronizedList(new ArrayList<>());
        unenrichedMessagesList = Collections.synchronizedList(new ArrayList<>());
    }

    public List<String> getEnrichedMessagesList() {
        return enrichedMessagesList;
    }

    public List<String> getUnenrichedMessagesList() {
        return unenrichedMessagesList;
    }

    @Override
    public String enrich(Message message) {
        EnrichedMessageFactory enrichedMessageFactory = enrichmentResolver.resolveEnrichmentType(message.getEnrichmentType());
        EnrichedMessage enrichedMessage = enrichedMessageFactory.enrich(message);

        if (enrichedMessage.isEnriched()) {
            enrichedMessagesList.add(enrichedMessage.getContent());
        } else {
            unenrichedMessagesList.add(enrichedMessage.getContent());
        }

        return enrichedMessage.getContent();
    }
}
