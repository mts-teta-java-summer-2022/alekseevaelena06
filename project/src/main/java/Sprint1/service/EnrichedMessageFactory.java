package Sprint1.service;

import Sprint1.models.Message;
import Sprint1.models.EnrichedMessage;

public interface EnrichedMessageFactory {
    EnrichedMessage enrich(Message message);
}
