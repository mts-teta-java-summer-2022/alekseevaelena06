package Sprint1.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@Builder(setterPrefix = "set")
@AllArgsConstructor
public class Enrichment {
    private String firstName;
    private String lastName;
}
