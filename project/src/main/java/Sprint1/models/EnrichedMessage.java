package Sprint1.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EnrichedMessage {
    private String content;
    private boolean isEnriched;
}