package Sprint1.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

@Getter
@AllArgsConstructor
public class Message {
    private final String content;
    private final EnrichmentType enrichmentType;

    public JSONObject getJSONMessage() throws ParseException {
        JSONParser jsonParser = new JSONParser();
        return (JSONObject) jsonParser.parse(this.getContent());
    }

    public enum EnrichmentType {
        MSISDN
    }
}
