package Sprint1.models;

import lombok.Getter;
import lombok.ToString;
import org.json.JSONObject;

import java.util.Objects;

@Getter
@ToString
public class MessageModel {
    private String action;
    private String page;
    private String msisdn;
    private Enrichment enrichment;

    public MessageModel(MessageModel.Builder builder) {
        this.action = builder.action;
        this.page = builder.page;
        this.msisdn = builder.msisdn;
        this.enrichment = builder.enrichment;
    }

    public static class Builder {

        private String action = "test";
        private String page = "test";
        private String msisdn;
        private Enrichment enrichment;
        private Enrichment.EnrichmentBuilder enrichmentBuilder = Enrichment.builder();

        public MessageModel.Builder setAction(String action) {
            this.action = action;
            return this;
        }

        public MessageModel.Builder setPage(String page) {
            this.page = page;
            return this;
        }

        public MessageModel.Builder setMsisdn(String msisdn) {
            this.msisdn = msisdn;
            return this;
        }

        public MessageModel.Builder setEnrichmentFirstName(String firstName) {
            enrichmentBuilder.setFirstName(firstName);

            return this;
        }

        public MessageModel.Builder setEnrichmentLastName(String lastName) {
            enrichmentBuilder.setLastName(lastName);

            return this;
        }

        public MessageModel build() {
            enrichment = enrichmentBuilder.build();

            return new MessageModel(this);
        }
    }

    public String getSendMessage(MessageModel message) {
        JSONObject json = new JSONObject();
        JSONObject enrichment = new JSONObject();

        enrichment.put("firstName", message.getEnrichment().getFirstName());
        enrichment.put("lastName", message.getEnrichment().getLastName());

        json.put("action", message.getAction());
        json.put("page", message.getPage());
        if (Objects.nonNull(message.getMsisdn())) {
            json.put("msisdn", message.getMsisdn());
        }
        json.put("enrichment", enrichment);

        return json.toString();
    }
}
