--1.Количество пользователей, которые не создали ни одного поста.
SELECT COUNT(prof1) FROM profile prof1 LEFT JOIN post
   ON  prof1.profile_id = post.profile_id
   WHERE post.profile_id  IS NULL;

--Ответ:
--5

--2.Выберите ID всех постов по возрастанию, у которых 2 комментария,
--title начинается с цифры, а длина content больше 20.

SELECT p1.post_id
FROM post p1
JOIN comment com ON p1.post_id = com.post_id
WHERE p1.title ~ '^[0-9]' and LENGTH(p1.content) > 20
GROUP BY p1.post_id
HAVING COUNT(com.comment_id) = 2
ORDER BY p1.post_id;

--Ответ:
--22
--24
--26
--28
--32
--34
--36
--38
--42
--44

--3.Выберите первые 3 ID постов по возрастанию, у которых либо нет комментариев, либо он один.

SELECT p1.post_id
FROM post p1
JOIN comment com ON p1.post_id = com.post_id
GROUP BY p1.post_id
HAVING COUNT(com.comment_id) <= 1
ORDER BY p1.post_id LIMIT 3;

--Ответ:
--1
--3
--5