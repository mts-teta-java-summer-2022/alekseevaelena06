package com.mts.teta;

public class TestData {
    public static final String URL_TEMPLATE = "/courses";
    public static final String URL_TEMPLATE_WITH_ID = "/courses/1";
    public static final String URL_TEMPLATE_WITH_TITLE_PREFIX = "/courses/titles?titlePrefix=Ja";
    public static final Long COURSE_ID = 1L;
    public static final String AUTHOR = "Test";
    public static final String TITLE_COURSE = "Java course";
    public static final String UPDATED_AUTHOR = "updated author";
    public static final String UPDATED_TITLE_COURSE = "Python course";
    public static final String TITLE_PREFIX = "Ja";
    public static final String PATH_FOR_ALL = "$";
    public static final String PATH_FOR_AUTHOR = "$[2].author";
    public static final String ERROR_DATE_PATH = "dateOccurred";
    public static final String ERROR_MESSAGE_PATH = "message";
    public static final String TOKEN = "afhjhjdhflkle34656354653";
}
