package com.mts.teta.service;

import com.mts.teta.mapper.UserMapper;
import com.mts.teta.persistence.CourseRepository;
import com.mts.teta.persistence.LessonRepository;
import com.mts.teta.persistence.RoleRepository;
import com.mts.teta.persistence.UserRepository;
import com.mts.teta.persistence.entity.Course;
import com.mts.teta.persistence.entity.Role;
import com.mts.teta.persistence.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class CourseServiceTest {
    CourseRepository courseRepositoryMock;
    CourseService courseService;
    UserRepository userRepositoryMock;
    LessonRepository lessonRepositoryMock;
    RoleRepository roleRepositoryMock;
    UserAuthService userAuthServiceMock;
    UserService userServiceMock;
    PasswordEncoder passwordEncoderMock;
    UserMapper userMapper;
    List<Course> courses = new ArrayList<>();

    @BeforeAll
    public void setUp() {
        courseRepositoryMock = Mockito.mock(CourseRepository.class);
        userRepositoryMock = Mockito.mock(UserRepository.class);
        userRepositoryMock = Mockito.mock(UserRepository.class);
        roleRepositoryMock = Mockito.mock(RoleRepository.class);
        userAuthServiceMock = Mockito.mock(UserAuthService.class);
        userServiceMock = Mockito.mock(UserService.class);
        passwordEncoderMock = Mockito.mock(PasswordEncoder.class);
        Mockito.when(userRepositoryMock.findAll()).thenReturn(Collections.emptyList());
        Mockito.when(roleRepositoryMock.findByName("admin")).thenReturn(Optional.of(new Role(1L, "admin", new HashSet<User>())));
        Mockito.when(passwordEncoderMock.encode(anyString())).thenReturn("md5hash");

        userServiceMock = new UserService(userRepositoryMock, passwordEncoderMock, userMapper);
        userAuthServiceMock = new UserAuthService(userRepositoryMock, roleRepositoryMock);
        courseService = new CourseService(courseRepositoryMock, lessonRepositoryMock, userRepositoryMock);

        Mockito.when(passwordEncoderMock.encode("superPass")).thenReturn("&$^%^");
        Mockito.when(passwordEncoderMock.encode("weak1234")).thenReturn("meh");

        courses.add(new Course("Petya", "Java"));
    }

    @Test
    void findAllCourses() {
        Mockito.when(courseRepositoryMock.findAll()).thenReturn(courses);
        List<Course> returnedCourses = courseService.findAllCourses();
        assertEquals(1, returnedCourses.size());
    }

    @Test
    void getById() {
        Mockito.when(courseRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(courses.get(0)));
        Course returnedCourse = courseService.getById(1L);
        assertNotNull(returnedCourse.getTitle());
    }

    @Test
    @DisplayName("Позитивный тест. Проверка сохранения курса.")
    void saveCourse() {
        Course courseRequest = new Course("Test", "Java course");
        Course course = new Course(1L,"Test", "Java course");
        when(courseRepositoryMock.save(any())).thenReturn(course);

        Course createdCourse = courseService.saveCourse(courseRequest);
        assertThat(createdCourse.getAuthor()).isSameAs(course.getAuthor());
        assertThat(createdCourse.getTitle()).isSameAs(course.getTitle());
        assertThat(1L).isSameAs(course.getId());
    }

    @Test
    void updateCourse() {
        Mockito.when(courseRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(courses.get(0)));
        courseService.updateCourse(courses.get(0).getId(), courses.get(0).getAuthor(), "newTitle");
        Mockito.verify(courseRepositoryMock, Mockito.times(1)).save(any());
    }

    @Test
    void deleteCourse() {
        Mockito.when(courseRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(courses.get(0)));
        courseService.deleteCourse(1L);
        Mockito.verify(courseRepositoryMock, Mockito.times(1)).delete(any());
    }
}