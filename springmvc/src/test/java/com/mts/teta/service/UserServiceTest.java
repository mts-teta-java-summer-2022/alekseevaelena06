package com.mts.teta.service;

import com.mts.teta.mapper.*;
import com.mts.teta.models.UserRq;
import com.mts.teta.models.dto.UserDto;
import com.mts.teta.persistence.RoleRepository;
import com.mts.teta.persistence.UserRepository;
import com.mts.teta.persistence.entity.Course;
import com.mts.teta.persistence.entity.Role;
import com.mts.teta.persistence.entity.User;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserServiceTest {
    UserRepository userRepositoryMock;
    RoleRepository roleRepositoryMock;
    UserAuthService userAuthServiceMock;
    UserService userService;
    PasswordEncoder passwordEncoderMock;
    UserMapper userMapper;
    List<User> defaultUserList = new ArrayList<>();

    @BeforeAll
    public void setUp() {
        userRepositoryMock = Mockito.mock(UserRepository.class);
        roleRepositoryMock = Mockito.mock(RoleRepository.class);
        userAuthServiceMock = Mockito.mock(UserAuthService.class);
        passwordEncoderMock = Mockito.mock(PasswordEncoder.class);
        Mockito.when(userRepositoryMock.findAll()).thenReturn(Collections.emptyList());
        Mockito.when(roleRepositoryMock.findByName("admin")).thenReturn(Optional.of(new Role(1L, "admin", new HashSet<User>())));
        Mockito.when(passwordEncoderMock.encode(anyString())).thenReturn("md5hash");

        userMapper = new UserMapperImpl();
        userService = new UserService(userRepositoryMock, passwordEncoderMock, userMapper);
        userAuthServiceMock = new UserAuthService(userRepositoryMock, roleRepositoryMock);

        Mockito.when(passwordEncoderMock.encode("superPass")).thenReturn("&$^%^");
        Mockito.when(passwordEncoderMock.encode("weak1234")).thenReturn("meh");

        Set<Course> courses = new HashSet<>();
        Set<Role> roles = new HashSet<>();
        courses.add(new Course("Petya", "Java"));
        roles.add(new Role(1L, "ROLE_STUDENT"));
        User user = new User(1L, "vasya", "1234", "true", courses, roles);
        defaultUserList.add(user);
    }

    @Test
    void allUsers() {
        Mockito.when(userRepositoryMock.findAll()).thenReturn(defaultUserList);
        List<User> userList = userService.allUsers();
        assertEquals(1, userList.size());
    }

    @Test
    void saveUser() {
        UserRq userRequest = new UserRq("vasya", "1234");
        Mockito.when(passwordEncoderMock.encode(userRequest.getPassword())).thenReturn("meh");
        Mockito.when(userRepositoryMock.save(any())).thenReturn(defaultUserList.get(0));
        User savedUser = userService.saveUser(userRequest);
        assertEquals(defaultUserList.get(0).getUsername(), savedUser.getUsername());
    }

    @Test
    void testSaveUser() {
        Mockito.when(passwordEncoderMock.encode(any())).thenReturn("meh");
        Mockito.when(userRepositoryMock.save(any())).thenReturn(defaultUserList.get(0));
        assertTrue(userService.saveUser(defaultUserList.get(0)));
    }

    @Test
    void deleteUser() {
        Mockito.when(userRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(defaultUserList.get(0)));
        userService.deleteUser(1L);
        Mockito.verify(userRepositoryMock, Mockito.times(1)).delete(defaultUserList.get(0));
    }

    @Test
    void getUserRoles() {
        Mockito.when(userRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(defaultUserList.get(0)));
        Set<Role> roles = userService.getUserRoles(1L);
        assertEquals(1, roles.size());
    }

    @Test
    void getUserDetailsByUsername() {
        Mockito.when(userRepositoryMock.findByUsername(any())).thenReturn(Optional.ofNullable(defaultUserList.get(0)));
        UserDto userDto = userService.getUserDetailsByUsername("vasya");
        assertNotNull(userDto);
    }
}