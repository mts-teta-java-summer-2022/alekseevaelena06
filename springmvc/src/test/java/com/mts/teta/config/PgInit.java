package com.mts.teta.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

@TestConfiguration
public class PgInit {
    private static final String PG_DOCKER_IMAGE = "postgres:13";
    private static final String PG_DOCKER_IMAGE_ORIGINAL = "postgres";
    private static final String INIT_SCRIPT_PATH = "db/changelog/initial/initial.sql";
    private static final DockerImageName DOCKER_IMAGE_NAME = DockerImageName.parse(PG_DOCKER_IMAGE).asCompatibleSubstituteFor(PG_DOCKER_IMAGE_ORIGINAL);
    @SuppressWarnings("resource")
    protected static PostgreSQLContainer<?> PG_CONTAINER = new PostgreSQLContainer<>(DOCKER_IMAGE_NAME)
            .withInitScript(INIT_SCRIPT_PATH);
    static {
        PG_CONTAINER.start();
        System.setProperty("spring.datasource.url", PG_CONTAINER.getJdbcUrl());
        System.setProperty("spring.liquibase.url", PG_CONTAINER.getJdbcUrl());
    }
}
