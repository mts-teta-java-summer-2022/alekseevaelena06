package com.mts.teta.controller;

import com.mts.teta.mapper.*;
import com.mts.teta.persistence.UserRepository;
import com.mts.teta.persistence.entity.User;
import com.mts.teta.service.UserAuthService;
import com.mts.teta.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(UserController.class)
class UserControllerTest {
    @Autowired
    MockMvc mvc;

    @MockBean
    UserRepository userRepositoryMock;

    @MockBean
    UserAuthService userAuthServiceMock;

    @MockBean
    UserService userServiceMock;

    @MockBean
    PasswordEncoder passwordEncoderMock;

    @TestConfiguration
    static class MapperTestConfig {
        @Bean
        public UserMapper getUserMapper() {
            return new UserMapperImpl();
        }
    }

    @Test
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    public void testUserForm() throws Exception {
        this.mvc.perform(get("/admin/user"))
                .andDo(print())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void createUser() throws Exception {
        Optional<User> user = userRepositoryMock.findByUsername("testUser");
        assertFalse(user.isPresent());

        mvc.perform(post("/admin/user")
                        .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                        .param("username", "testUser")
                        .param("password", "testPass")
                        .with(csrf()))
                .andExpect(status().is3xxRedirection())
                .andExpect(view().name("redirect:/user"));

        user = userRepositoryMock.findByUsername("testUser");
        assertTrue(user.isPresent());
    }
}