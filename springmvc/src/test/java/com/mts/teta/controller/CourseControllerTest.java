package com.mts.teta.controller;

import com.google.gson.Gson;
import com.mts.teta.mapper.*;
import com.mts.teta.models.CourseRq;
import com.mts.teta.persistence.UserRepository;
import com.mts.teta.persistence.entity.Course;
import com.mts.teta.service.CourseService;
import com.mts.teta.service.UserAuthService;
import com.mts.teta.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.List;

import static com.mts.teta.TestData.*;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(CourseController.class)
class CourseControllerTest {
    Gson gson = new Gson();
    List<Course> courses = new ArrayList<>();
    Course COURSE_1 = new Course(1L, AUTHOR, TITLE_COURSE);
    Course COURSE_2 = new Course(2L, AUTHOR, TITLE_COURSE);
    Course COURSE_3 = new Course(3L, AUTHOR, TITLE_COURSE);

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CourseService courseServiceMock;

    @MockBean
    UserRepository userRepositoryMock;

    @MockBean
    UserAuthService userAuthServiceMock;

    @MockBean
    UserService userServiceMock;

    @MockBean
    PasswordEncoder passwordEncoderMock;

    @TestConfiguration
    static class MapperTestConfig {
        @Bean
        public CourseMapper getCourseMapper() {
            return new CourseMapperImpl(new UserMapperImpl());
        }

        @Bean
        public LessonMapper getLessonMapper() {
            return new LessonMapperImpl();
        }
    }

    @Test
    @DisplayName("Позитивный тест. Проверка получения всех курсов.")
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void findAllCourses() throws Exception {
        courses.add(COURSE_1);
        courses.add(COURSE_2);
        courses.add(COURSE_3);
        Mockito.when(courseServiceMock.findAllCourses()).thenReturn(courses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get(URL_TEMPLATE)
                        .header("token", TOKEN)
                        .header("userId", 7)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(PATH_FOR_ALL, hasSize(3)))
                .andExpect(jsonPath(PATH_FOR_AUTHOR, is(AUTHOR)));
    }

    @Test
    @DisplayName("Позитивный тест. Создание курса.")
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void saveCourse() throws Exception {
        Mockito.when(courseServiceMock.saveCourse(new Course(AUTHOR, TITLE_COURSE))).thenReturn(COURSE_1);
        String json = gson.toJson(new CourseRq(AUTHOR, TITLE_COURSE));

        mockMvc.perform(MockMvcRequestBuilders
                        .post(URL_TEMPLATE)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk())
                .andExpect(jsonPath("author", is(AUTHOR)))
                .andExpect(jsonPath("title", is(TITLE_COURSE)));
    }

    @Test
    @DisplayName("Позитивный тест. Проверка обновления курса.")
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void updateCourse() throws Exception {
        Mockito.doNothing().when(courseServiceMock).updateCourse(COURSE_ID, UPDATED_AUTHOR, UPDATED_TITLE_COURSE);
        String json = gson.toJson(new CourseRq(UPDATED_AUTHOR, UPDATED_TITLE_COURSE));

        mockMvc.perform(MockMvcRequestBuilders
                        .put(URL_TEMPLATE_WITH_ID)
                        .contentType(MediaType.APPLICATION_JSON).content(json))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Позитивный тест. Проверка удаления курса.")
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void deleteCourse() throws Exception {
        Mockito.doNothing().when(courseServiceMock).deleteCourse(COURSE_ID);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete(URL_TEMPLATE_WITH_ID))
                .andExpect(status().isOk());
    }

    @Test
    @DisplayName("Позитивный тест. Получение курса по названию.")
    @WithMockUser(value = "admin", password = "1234", roles = {"ADMIN"})
    void getCoursesByTitle() throws Exception {
        courses.add(COURSE_1);
        courses.add(COURSE_2);
        courses.add(COURSE_3);
        Mockito.when(courseServiceMock.findByTitlePrefix(TITLE_PREFIX)).thenReturn(courses);

        mockMvc.perform(MockMvcRequestBuilders
                        .get(URL_TEMPLATE_WITH_TITLE_PREFIX)
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath(PATH_FOR_ALL, hasSize(3)))
                .andExpect(jsonPath(PATH_FOR_AUTHOR, is(AUTHOR)));
    }
}