package com.mts.teta;

import com.mts.teta.config.PgInit;
import com.mts.teta.models.UserRq;
import com.mts.teta.persistence.UserRepository;
import com.mts.teta.persistence.entity.User;
import com.mts.teta.service.UserService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@Import(PgInit.class)
@SpringBootTest
@ActiveProfiles("test")
public class SpringMvcAppTest {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @DisplayName("Проверка работы получения user по username в репозитории. Интеграционный тест.")
    @Test
    void testFindUserByUserName() {
        User user = userService.saveUser(new UserRq("vasya", "1234"));
        Optional<User> userFromDb = userRepository.findByUsername(user.getUsername());
        assertNotNull(userFromDb);
    }
}
