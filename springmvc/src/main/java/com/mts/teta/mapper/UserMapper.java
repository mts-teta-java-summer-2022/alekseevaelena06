package com.mts.teta.mapper;

import com.mts.teta.models.dto.UserDto;
import com.mts.teta.models.UserRq;
import com.mts.teta.persistence.entity.User;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface UserMapper {
    UserDto toDto(User user);

    User toUser(UserRq userRequest);
}
