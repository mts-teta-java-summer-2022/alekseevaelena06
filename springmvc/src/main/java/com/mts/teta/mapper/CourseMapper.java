package com.mts.teta.mapper;

import com.mts.teta.models.dto.CourseDescriptionDto;
import com.mts.teta.models.dto.CourseDto;
import com.mts.teta.models.CourseRq;
import com.mts.teta.persistence.entity.Course;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {UserMapper.class}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface CourseMapper {

    CourseDto toDto(Course course);

    Course toCourse(CourseRq courseRq);

    CourseDescriptionDto toCourseDescriptionDto(Course course);
}
