package com.mts.teta.mapper;

import com.mts.teta.models.dto.LessonDto;
import com.mts.teta.persistence.entity.Lesson;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface LessonMapper {
    LessonDto toDto(Lesson lesson);
}
