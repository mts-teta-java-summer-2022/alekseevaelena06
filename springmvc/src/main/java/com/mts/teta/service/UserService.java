package com.mts.teta.service;

import com.mts.teta.mapper.UserMapper;
import com.mts.teta.models.UserRq;
import com.mts.teta.models.dto.UserDto;
import com.mts.teta.persistence.entity.Role;
import com.mts.teta.persistence.entity.User;
import com.mts.teta.persistence.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Component
@AllArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final PasswordEncoder bCryptPasswordEncoder;
    private final UserMapper userMapper;

    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public User saveUser(UserRq userRequest){
        User user = new User();
        user.setUsername(userRequest.getUserName());
        user.setPassword(bCryptPasswordEncoder.encode(userRequest.getPassword()));
        return userRepository.save(user);
    }

    public boolean saveUser(User user) {
        Optional<User> userFromDB = userRepository.findByUsername(user.getUsername());

        if (userFromDB.isPresent()) {
            return false;
        }

        user.setRoles(Collections.singleton(new Role(1L, "ROLE_STUDENT")));
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));

        userRepository.save(user);
        return true;
    }

    public void deleteUser(Long userId) {
        User user = userRepository.findById(userId).orElseThrow();
        userRepository.delete(user);
    }

    public Set<Role> getUserRoles(Long userId){
        User user = userRepository.findById(userId).orElseThrow();
        return user.getRoles();
    }

    public UserDto getUserDetailsByUsername(String username) {
        return userMapper.toDto(userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found")));
    }
}
