package com.mts.teta.service;

import com.mts.teta.models.dto.LessonDto;
import com.mts.teta.persistence.entity.Course;
import com.mts.teta.persistence.entity.Lesson;
import com.mts.teta.persistence.entity.User;
import com.mts.teta.persistence.CourseRepository;
import com.mts.teta.persistence.LessonRepository;
import com.mts.teta.persistence.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.NoSuchElementException;

@Component
public class CourseService {
    private final CourseRepository courseRepository;
    private final LessonRepository lessonRepository;
    private final UserRepository userRepository;

    @Autowired
    public CourseService(CourseRepository courseRepository, LessonRepository lessonRepository, UserRepository userRepository) {
        this.courseRepository = courseRepository;
        this.lessonRepository = lessonRepository;
        this.userRepository = userRepository;
    }

    public List<Course> findAllCourses() {
        return courseRepository.findAll();
    }

    public Course getById(Long id) {
        return courseRepository.findById(id).orElseThrow();
    }

    public Course saveCourse(Course course) {
        return courseRepository.save(course);
    }

    @Transactional
    public void updateCourse(Long id, String author, String title) {
        Course course = courseRepository.findById(id).orElseThrow();
        course.setAuthor(author);
        course.setTitle(title);
        courseRepository.save(course);
    }

    public void deleteCourse(Long id) {
        Course course = courseRepository.findById(id).orElseThrow();
        courseRepository.delete(course);
    }

    public List<Course> findByTitlePrefix(String titlePrefix) {
        return courseRepository.findByTitleStartsWith(titlePrefix);
    }

    public Lesson saveLesson(Long id, String title, String text) {
        Course course = courseRepository.findById(id).orElseThrow();
        Lesson lesson = new Lesson(title, text, course);
        return lessonRepository.save(lesson);
    }

    @Transactional
    public Lesson updateLesson(LessonDto lessonDto) {
        Course course = courseRepository.findById(lessonDto.getCourseId()).orElseThrow();
        Lesson lesson = course.getLessons().stream()
                .filter(l -> lessonDto.getId().equals(l.getId()))
                .findAny()
                .orElseThrow((() -> new NoSuchElementException("There is no lesson with id:" + lessonDto.getId())));
        lesson.setTitle(lessonDto.getTitle());
        lesson.setText(lessonDto.getText());
        return lessonRepository.save(lesson);
    }

    @Transactional
    public void deleteLesson(Long id, Long lessonId) {
        Course course = courseRepository.findById(id).orElseThrow();
        Lesson lesson = course.getLessons().stream()
                .filter(l -> lessonId.equals(l.getId()))
                .findAny()
                .orElseThrow((() -> new NoSuchElementException("There is no lesson with id:" + lessonId)));
        lessonRepository.delete(lesson);
        course.getLessons().remove(lesson);
        courseRepository.save(course);
    }

    @Transactional
    public void assignUser(Long id, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(NoSuchElementException::new);
        Course course = courseRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        course.getUsers().add(user);
        user.getCourses().add(course);
        courseRepository.save(course);
    }

    @Transactional
    public void removeUser(Long id, Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(NoSuchElementException::new);
        Course course = courseRepository.findById(id)
                .orElseThrow(NoSuchElementException::new);
        course.getUsers().remove(user);
        courseRepository.save(course);
    }
}
