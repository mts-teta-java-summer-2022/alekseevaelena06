package com.mts.teta.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.OffsetDateTime;

@Setter
@Getter
@AllArgsConstructor
public class ApiError {
    OffsetDateTime dateOccurred;
    String message;
}
