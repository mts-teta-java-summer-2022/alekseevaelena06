package com.mts.teta.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class LessonRq {
    @NotBlank(message = "Lesson title has to be filled")
    @Size(min = 2, message = "Lesson title must not be less than 2 characters")
    private String title;
    @NotBlank(message = "Lesson text has to be filled")
    @Size(min = 5, message = "Lesson text must not be less than 5 characters")
    private String text;
}
