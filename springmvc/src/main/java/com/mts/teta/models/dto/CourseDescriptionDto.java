package com.mts.teta.models.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CourseDescriptionDto {
    private Long id;
    private String author;
    private String title;
    private Set<UserDto> users;
}
