package com.mts.teta.models;

import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserRq {
    @NotBlank(message = "Field userName has to be filled")
    @Size(min = 2, message = "userName must not be less than 2 characters")
    private String userName;

    @NotBlank(message = "Field password has to be filled")
    @Size(min = 2, message = "password must not be less than 2 characters")
    private String password;
}
