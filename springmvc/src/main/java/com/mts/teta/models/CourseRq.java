package com.mts.teta.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@Setter
@AllArgsConstructor
public class CourseRq {
    @NotBlank(message = "Course author has to be filled")
    @Size(min = 2, message = "author must not be less than 2 characters")
    private String author;
    @NotBlank(message = "Course title has to be filled")
    @Size(min = 2, message = "title must not be less than 2 characters")
    private String title;
}
