package com.mts.teta.controller;

import com.mts.teta.mapper.CourseMapper;
import com.mts.teta.mapper.LessonMapper;
import com.mts.teta.models.CourseRq;
import com.mts.teta.models.LessonRq;
import com.mts.teta.models.dto.CourseDescriptionDto;
import com.mts.teta.models.dto.CourseDto;
import com.mts.teta.models.dto.LessonDto;
import com.mts.teta.service.CourseService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNullElse;

@RestController
@RequestMapping("/courses")
@RequiredArgsConstructor
public class CourseController {
    private final CourseService courseService;
    private final CourseMapper courseMapper;
    private final LessonMapper lessonMapper;

    @GetMapping
    public List<CourseDto> findAllCourses(HttpSession session) {
        return courseService.findAllCourses()
                .stream()
                .map(courseMapper::toDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping
    public CourseDto saveCourse(@Valid @RequestBody CourseRq request) {
        return courseMapper.toDto(
                courseService.saveCourse(courseMapper.toCourse(request)));
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}")
    public void updateCourse(@PathVariable Long id,
                             @Valid @RequestBody CourseRq request) {
        courseService.updateCourse(id, request.getAuthor(), request.getTitle());
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}")
    public void deleteCourse(@PathVariable Long id) {
        courseService.deleteCourse(id);
    }

    @GetMapping("/titles")
    public List<CourseDto> getCoursesByTitle(@RequestParam(name = "titlePrefix", required = false) String titlePrefix) {
        return courseService.findByTitlePrefix(requireNonNullElse(titlePrefix, ""))
                .stream()
                .map(courseMapper::toDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PostMapping("/{id}/lesson")
    public ResponseEntity<LessonDto> createLesson(@PathVariable Long id,
                                                  @Valid @RequestBody LessonRq request) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(lessonMapper.toDto(courseService.saveLesson(id, request.getTitle(), request.getText())));
    }

    @GetMapping("/{id}/lesson")
    @Transactional(readOnly = true)
    public List<LessonDto> getLessons(@PathVariable Long id) {
        return courseService.getById(id).getLessons().stream()
                .map(lessonMapper::toDto)
                .collect(Collectors.toList());
    }

    @Secured("ROLE_ADMIN")
    @PutMapping("/{id}/lesson/{lessonId}")
    public void updateLesson(@PathVariable Long id,
                             @PathVariable Long lessonId,
                             @Valid @RequestBody LessonRq request) {
        courseService.updateLesson(new LessonDto(lessonId, request.getTitle(), request.getText(), id));
    }

    @Secured("ROLE_ADMIN")
    @DeleteMapping("/{id}/lesson/{lessonId}")
    public void deleteLesson(@PathVariable Long id,
                             @PathVariable Long lessonId) {
        courseService.deleteLesson(id, lessonId);
    }

    @PostMapping("/{id}/assign")
    @ResponseStatus(code = HttpStatus.CREATED)
    public void assignUser(@PathVariable("id") Long id,
                           @RequestParam("userId") Long userId) {
        courseService.assignUser(id, userId);
    }

    @DeleteMapping("/{id}/remove")
    public void removeUser(@PathVariable("id") Long id,
                           @RequestParam("userId") Long userId) {
        courseService.removeUser(id, userId);
    }

    @GetMapping("/{id}/description")
    public CourseDescriptionDto getCourseDescription(@PathVariable("id") Long id) {
        return courseMapper.toCourseDescriptionDto(courseService.getById(id));
    }
}
