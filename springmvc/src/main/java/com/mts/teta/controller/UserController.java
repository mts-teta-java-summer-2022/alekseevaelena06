package com.mts.teta.controller;

import com.mts.teta.mapper.UserMapper;
import com.mts.teta.models.dto.UserDto;
import com.mts.teta.models.UserRq;
import com.mts.teta.persistence.entity.Role;
import com.mts.teta.service.UserAuthService;
import com.mts.teta.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/admin")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;
    private final UserMapper userMapper;
    private final UserAuthService userAuthService;

    @GetMapping("/index")
    public String manageUsers() {
        return "index";
    }

    @GetMapping("/user")
    public List<UserDto> getUsers() {
        return userService.allUsers().stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @PostMapping("/user")
    @ResponseStatus(code = HttpStatus.CREATED)
    public UserDto createUser(@Valid @RequestBody UserRq userRequest) {
        return userMapper.toDto(userService.saveUser(userRequest));
    }

    @DeleteMapping("/user/{id}")
    public void deleteUser(@PathVariable Long id) {
        userService.deleteUser(id);
    }


    @GetMapping("/user/{id}/role")
    public List<String> getUserRoles(@PathVariable Long id){
        return userService.getUserRoles(id).stream().map(Role::getName).collect(Collectors.toList());
    }

    @PostMapping("/user/{id}/role/{roleName}")
    public List<String> addUserRole(@PathVariable Long id, @PathVariable String roleName){
        return userAuthService.addUserRole(id, roleName).stream().map(Role::getName).collect(Collectors.toList());
    }

    @DeleteMapping("/user/{id}/role/{roleName}")
    public List<String> deleteUserRole(@PathVariable Long id, @PathVariable String roleName){
        return userAuthService.deleteUserRole(id, roleName).stream().map(Role::getName).collect(Collectors.toList());
    }
}
