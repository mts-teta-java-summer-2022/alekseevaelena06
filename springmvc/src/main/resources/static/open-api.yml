---
openapi: 3.0.1
info:
  title: mts-coursera
  description: MTS TETA JAVA SUMMER 2022
  version: 1.0.0
servers:
  - url: http://localhost:8080
    description: При локальном запуске
paths:
  "/mts-coursera/courses/{id}":
    put:
      tags:
        - course-controller
      operationId: updateCourse
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/CourseRq"
        required: true
      responses:
        '200':
          description: OK
    delete:
      tags:
        - course-controller
      operationId: deleteCourse
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
  "/mts-coursera/courses/{id}/lesson/{lessonId}":
    put:
      tags:
        - course-controller
      operationId: updateLesson
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
        - name: lessonId
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/LessonRq"
        required: true
      responses:
        '200':
          description: OK
    delete:
      tags:
        - course-controller
      operationId: deleteLesson
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
        - name: lessonId
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
  "/mts-coursera/user":
    get:
      tags:
        - user-controller
      operationId: getUsers
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/UserDto"
    post:
      tags:
        - user-controller
      operationId: createUser
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/UserRq"
        required: true
      responses:
        '201':
          description: Created
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/UserDto"
  "/mts-coursera/courses":
    get:
      tags:
        - course-controller
      operationId: findAllCourses
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/CourseDto"
    post:
      tags:
        - course-controller
      operationId: saveCourse
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/CourseRq"
        required: true
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/CourseDto"
  "/mts-coursera/courses/{id}/lesson":
    get:
      tags:
        - course-controller
      operationId: getLessons
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/LessonDto"
    post:
      tags:
        - course-controller
      operationId: createLesson
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/LessonRq"
        required: true
      responses:
        '201':
          description: Created
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/LessonDto"
  "/mts-coursera/courses/{id}/assign":
    post:
      tags:
        - course-controller
      operationId: assignUser
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
        - name: userId
          in: query
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '201':
          description: Created
  "/mts-coursera/courses/{id}/description":
    get:
      tags:
        - course-controller
      operationId: getCourseDescription
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                "$ref": "#/components/schemas/CourseDescriptionDto"
  "/mts-coursera/courses/titles":
    get:
      tags:
        - course-controller
      operationId: getCoursesByTitle
      parameters:
        - name: titlePrefix
          in: query
          required: false
          schema:
            type: string
      responses:
        '200':
          description: OK
          content:
            "*/*":
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/CourseDto"
  "/mts-coursera/user/{id}":
    delete:
      tags:
        - user-controller
      operationId: deleteUser
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
  "/mts-coursera/courses/{id}/remove":
    delete:
      tags:
        - course-controller
      operationId: removeUser
      parameters:
        - name: id
          in: path
          required: true
          schema:
            type: integer
            format: int64
        - name: userId
          in: query
          required: true
          schema:
            type: integer
            format: int64
      responses:
        '200':
          description: OK
components:
  schemas:
    CourseRq:
      required:
        - author
        - title
      type: object
      properties:
        author:
          maxLength: 2147483647
          minLength: 2
          type: string
        title:
          maxLength: 2147483647
          minLength: 2
          type: string
    LessonRq:
      required:
        - text
        - title
      type: object
      properties:
        title:
          maxLength: 2147483647
          minLength: 2
          type: string
        text:
          maxLength: 2147483647
          minLength: 5
          type: string
    UserRq:
      required:
        - userName
      type: object
      properties:
        userName:
          maxLength: 2147483647
          minLength: 2
          type: string
    UserDto:
      type: object
      properties:
        id:
          type: integer
          format: int64
        userName:
          type: string
    CourseDto:
      type: object
      properties:
        id:
          type: integer
          format: int64
        author:
          type: string
        title:
          type: string
    LessonDto:
      type: object
      properties:
        id:
          type: integer
          format: int64
        title:
          type: string
        text:
          type: string
        courseId:
          type: integer
          format: int64
    CourseDescriptionDto:
      type: object
      properties:
        id:
          type: integer
          format: int64
        author:
          type: string
        title:
          type: string
        users:
          uniqueItems: true
          type: array
          items:
            "$ref": "#/components/schemas/UserDto"
