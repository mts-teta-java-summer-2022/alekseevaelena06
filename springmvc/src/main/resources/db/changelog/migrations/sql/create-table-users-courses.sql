CREATE TABLE IF NOT EXISTS public.USERS_COURSES
(
    user_id     BIGINT    NOT NULL,
    course_id   BIGINT    NOT NULL
);

ALTER TABLE public.USERS_COURSES
  ADD CONSTRAINT fk_users_courses_user
      FOREIGN KEY (user_id)
      REFERENCES users (id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE public.USERS_COURSES
  ADD CONSTRAINT fk_users_courses_course
      FOREIGN KEY (course_id)
      REFERENCES courses (id)
      DEFERRABLE INITIALLY DEFERRED;

GRANT delete, insert, references, select, trigger, update ON TABLE public.USERS_COURSES to mts_coursera_app;