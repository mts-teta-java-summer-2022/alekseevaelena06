CREATE TABLE IF NOT EXISTS public.ROLES
(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    name     VARCHAR(255)                 NOT NULL
);

GRANT delete, insert, references, select, trigger, update ON TABLE public.ROLES to mts_coursera_app;

INSERT INTO public.roles(name)
VALUES ('ROLE_STUDENT'), ('ROLE_ADMIN');