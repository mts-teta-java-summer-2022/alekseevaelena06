CREATE TABLE IF NOT EXISTS public.USERS
(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    username     VARCHAR(255)                 NOT NULL
);

GRANT delete, insert, references, select, trigger, update ON TABLE public.USERS to mts_coursera_app;
