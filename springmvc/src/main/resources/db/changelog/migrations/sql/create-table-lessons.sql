CREATE TABLE IF NOT EXISTS public.LESSONS
(
    id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    title       VARCHAR(255)             NOT NULL,
    course_id   BIGINT                   NOT NULL,
    text        TEXT                     NOT NULL
);

ALTER TABLE public.LESSONS
  ADD CONSTRAINT fk_lesson_course
      FOREIGN KEY (course_id)
      REFERENCES courses (id)
      ON DELETE CASCADE
      ON UPDATE RESTRICT
      DEFERRABLE INITIALLY DEFERRED;

GRANT delete, insert, references, select, trigger, update ON TABLE public.LESSONS to mts_coursera_app;
