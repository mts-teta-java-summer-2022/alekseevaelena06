CREATE TABLE IF NOT EXISTS public.COURSES (
   id BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
   author   VARCHAR(255)                NOT NULL,
   title    VARCHAR(255)                NOT NULL
);

GRANT delete, insert, references, select, trigger, update ON TABLE public.COURSES to mts_coursera_app;