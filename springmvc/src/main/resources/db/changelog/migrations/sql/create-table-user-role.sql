CREATE TABLE IF NOT EXISTS public.USER_ROLE
(
    user_id   BIGINT    NOT NULL,
    role_id   BIGINT    NOT NULL
);

ALTER TABLE public.USER_ROLE
  ADD CONSTRAINT fk_user_role_user
      FOREIGN KEY (user_id)
      REFERENCES users (id)
      DEFERRABLE INITIALLY DEFERRED;

ALTER TABLE public.USER_ROLE
  ADD CONSTRAINT fk_user_role_role
      FOREIGN KEY (role_id)
      REFERENCES roles (id)
      DEFERRABLE INITIALLY DEFERRED;

GRANT delete, insert, references, select, trigger, update ON TABLE public.USER_ROLE to mts_coursera_app;